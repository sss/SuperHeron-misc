# Copyright 2009-2013 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop freedesktop-mime gtk-icon-cache mono
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.13 1.12 1.11 ] ]

SUMMARY="Integrated Development Environment for .NET"
DESCRIPTION="
MonoDevelop is an IDE primarily designed for C# and other .NET languages.
MonoDevelop enables developers to quickly write desktop and ASP.NET Web
applications on Linux. MonoDevelop makes it easy for developers to port
.NET applications created with Visual Studio to Linux and to maintain
a single code base for all platforms.
"
HOMEPAGE="http://monodevelop.com"
DOWNLOADS="
    http://download.mono-project.com/sources/${PN}/${PNV}.tar.bz2
    https://launchpadlibrarian.net/153448659/NUnit-2.6.3.zip
    https://launchpadlibrarian.net/68057829/NUnit-2.5.10.11092.zip
    https://github.com/mono/nuget-binary/archive/master.zip
    https://www.nuget.org/api/v2/package/Microsoft.AspNet.Mvc/5.2.2 -> Microsoft.AspNet.Mvc.5.2.2.nupkg
    https://www.nuget.org/api/v2/package/Microsoft.Web.Infrastructure/1.0.0.0 -> Microsoft.Web.Infrastructure.1.0.0.0.nupkg
    https://www.nuget.org/api/v2/package/Microsoft.AspNet.Razor/3.2.2 -> Microsoft.AspNet.Razor.3.2.2.nupkg
    https://www.nuget.org/api/v2/package/Microsoft.AspNet.WebPages/3.2.2 -> Microsoft.AspNet.WebPages.3.2.2.nupkg
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="extensions git subversion"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-dotnet/mono-addins[>=0.6][gui]
        dev-lang/mono[>=2.10.9]
        gnome-bindings/gnome-sharp:2[>=2.12.8]
        gnome-bindings/gtk-sharp:2[>=2.12.8]
        git? ( dev-scm/git )
        subversion? ( dev-scm/subversion )
"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/Documentation"
UPSTREAM_RELEASE_NOTES="http://developer.xamarin.com/releases/studio/xamarin.studio_5.5/xamarin.studio_5.5/"

WORK="${WORKBASE}/${PN}-$(ever range 1-3)"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}/${PNV}-fix-libdir.patch" )

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--disable-update-desktopdb'
    '--disable-update-mimedb'
    '--enable-gnomeplatform'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'extensions monoextensions'
    'git'
    'subversion'
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--any --enable-tests' )

DEFAULT_SRC_COMPILE_PARAMS=( '-j1' )

src_prepare() {
    # Do not look for version.config in parent directory. It's located in ${WORK}.
    edo sed -in "s/..\/version.config/version.config/g" configure.in

    # Use the right macro to find pkg-config
    edo sed -e "s/AC_PATH_PROG(PKG_CONFIG, pkg-config, no)/PKG_PROG_PKG_CONFIG/" -i configure.in

    # Install arch-independent files to the right place, i.e. /usr/share
    edo sed -e "s|\$(prefix)/share/locale|\$(localedir)|" -i po/Makefile.am

    # Add missing build dependencies
    edo mkdir -p packages/NUnit.2.6.3/lib
    edo mkdir -p packages/NUnit.Runners.2.6.3/tools/lib
    edo cp -fR ../NUnit-2.6.3/bin/framework/* packages/NUnit.2.6.3/lib
    edo cp -fR ../NUnit-2.6.3/bin/lib/* packages/NUnit.Runners.2.6.3/tools/lib
    edo cp -fR ../NUnit-2.5.10.11092/bin/net-2.0/framework/* external/cecil/Test/libs/nunit-2.5.10
    edo cp -fR ../nuget-binary-master/* external/nuget-binary/
    edo unzip "${FETCHEDDIR}/Microsoft.AspNet.Mvc.5.2.2.nupkg" -d packages/Microsoft.AspNet.Mvc.5.2.2
    edo unzip "${FETCHEDDIR}/Microsoft.Web.Infrastructure.1.0.0.0.nupkg" -d packages/Microsoft.Web.Infrastructure.1.0.0.0
    edo unzip "${FETCHEDDIR}/Microsoft.AspNet.Razor.3.2.2.nupkg" -d packages/Microsoft.AspNet.Razor.3.2.2
    edo unzip "${FETCHEDDIR}/Microsoft.AspNet.WebPages.3.2.2.nupkg" -d packages/Microsoft.AspNet.WebPages.3.2.2

    autotools_src_prepare
}

# Update desktop and MIME databases at the same time
pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

